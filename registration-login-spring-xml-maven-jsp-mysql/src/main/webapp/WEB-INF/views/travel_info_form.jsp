<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add New Business Trip</h1>
       <form:form method="post" action="saveTrip?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data">  
      	<table >  
          <tr>  
          <td>Employee_id : </td> 
          <td><form:input path="employee_id"  /></td>
         </tr> 
         <tr>  
          <td>Add description : </td> 
          <td><form:input path="description"  /></td>
         </tr>  
         <tr>  
          <td>Add start date :</td>  
          <td><form:input type =  "date" path="start_date" /></td>
         </tr> 
         <tr>  
          <td>Add end date:</td>  
          <td><form:input type = "date" path="end_date" /></td>
         </tr> 
          <tr>  
          <td>Is confirmed:</td>  
          <td><form:checkbox path="confirmed"  disabled = "true"/></td>
         </tr> 
          <tr>  
          <td>Add number of days :</td>  
          <td><form:input path="number_of_days" onchange="calculateAllowance()"  /></td>
         </tr>
          <tr>  
          <td>Add number of nights :</td>  
          <td><form:input path="number_of_night" disabled = "true"/></td>
         </tr>
          <tr>  
          <td>Daily allowance :</td>  
          <td><form:input  path="daily_allowance"  value = "50" disabled = "true" /></td>
         </tr>
         <tr>  
          <td>Total allowance :</td>  
          <td><form:input path="total_allowance" disabled = "true" /></td>
         </tr>
          <tr>  
          <td>Add transport cost :</td>  
          <td><form:input path="transport_cost" /></td>
         </tr>
          <tr>  
          <td>Add accommodation costs :</td>  
          <td><form:input path="accommodation_costs" /></td>
         </tr>
          <tr>  
          <td>Add other costs :</td>  
          <td><form:input path="other_costs" /></td>
         </tr>
         <tr>  
          <td>Add comments :</td>  
          <td><form:input path="comments" /></td>
         </tr>
          <tr>
          <td>Add photo :</td> 
          <td colspan="2">File to upload <input type="file" name="file"></td> 
         </tr>
         
         
         <tr>  </tr> 
         <tr> 
          <td></td><td><input type="submit" value="Save" />
          
          <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='user'">Home</button>
          <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='viewtrips'">View all business trips</button>
          </td>  
         </tr>  
        </table>  
       </form:form>  
        <script>
        function calculateAllowance() {
			document.getElementById("total_allowance").value = document.getElementById("daily_allowance").value  * document.getElementById("number_of_days").value; 
			document.getElementById("number_of_night").value = document.getElementById("number_of_days").value - 1; 

        }
//   �he k�lge ei saa mitut meetodit teha!      
//function calculateNights() {
// 	document.getElementById("number_of_night").value = document.getElementById("number_of_days").value - 1; 
//    }

       
    </script> 
