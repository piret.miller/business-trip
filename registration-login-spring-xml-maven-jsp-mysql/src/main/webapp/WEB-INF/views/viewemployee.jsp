    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
    
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    
    
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Employees List</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    	<style>
	    .center {
	  text-align: center;
/* 	  color: red; */
	}
	</style>
          
</head>
<body>


	<h1>Employees List</h1>
	<table id="dtOrderExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
	
	<tr><th class="center">Id</th><th>First name</th><th>Last name</th><th>Personal code</th><th class="center">Is active</th><th  class="center">User id</th><th class="center">Edit</th><th class="center">Delete</th></tr>
    <c:forEach var="employee" items="${list}"> 
    <tr>
    <td class="center">${employee.id}</td>
    <td>${employee.first_name}</td>
    <td>${employee.last_name}</td>
    <td>${employee.personal_code}</td>
    <td class="center">${employee.active}</td>
    <td class="center">${employee.user_id}</td>
    <td  class="center"><a href="editemployee/${employee.id}">Edit</a></td>
    <td class="center"><a href="deleteemployee/${employee.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    
<!--     <a href="employeeform">Add New Employee</a> -->
    
      <div xmlns:th="http://www.thymeleaf.org"
     style="text-align: right; border: 0px solid #ccc;padding:5px;margin-top:10px;width:250px">
       
    <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='employeeform'">Add New Employee</button>
 
     </div>  