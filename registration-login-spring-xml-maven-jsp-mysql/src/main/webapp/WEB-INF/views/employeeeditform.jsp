<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Edit Employee</h1>
       <form:form method="POST" action="/account/editsave?${_csrf.parameterName}=${_csrf.token}">  
      	<table >  
      	<tr>
      	<td></td>  
         <td><form:hidden  path="id" /></td>
         </tr> 
         <tr>  
          <td>First name :</td>  
          <td><form:input path="first_name" /></td>
         </tr>
         <tr>  
          <td>Last name :</td>  
          <td><form:input path="last_name" /></td>
         </tr>
         <tr>  
          <td>Personal code :</td>  
          <td><form:input path="personal_code" /></td>
         </tr> 
         <tr>  
          <td>Is active :</td>  
          <td><form:checkbox path="active" /></td>
         </tr> 
          <tr>  
          <td>User :</td>  
          <td><form:select path="user_id" items="${users}" /></td>
         </tr>

         
         <tr>  
          <td> </td>  
          <td><input type="submit" value="Edit Save" />
          <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='/account/viewemployee'">Back</button>
          </td> </td>  
         </tr>  
         </table>  
      	</form:form>  
