<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>View Trip</h1>
		<form:form method="POST" action="/account/viewsingletrip?${_csrf.parameterName}=${_csrf.token}">
       <img width="100" height="100" 
       src="../getPhoto/<c:out value='${command.id}'/>">
      	<table >  
      	 <tr>  
          <td>Employee id : </td> 
          <td>${command.employee_id}</td>
         </tr> 
         <tr>  
          <td>Add description : </td> 
          <td>${command.description}</td>
         </tr>  
         <tr>  
          <td>Add start date :</td>  
          <td>${command.start_date}</td>
         </tr> 
         <tr>  
          <td>Add end date:</td>  
          <td>${command.end_date}</td>
         </tr> 
          <tr>  
          <td>Is confirmed:</td>  
          <td>${command.confirmed}</td>
         </tr> 
          <tr>  
          <td>Add number of days :</td>  
          <td>${command.number_of_days}</td>
         </tr>
          <tr>  
          <td>Add number of nights :</td>  
          <td>${command.number_of_night}</td>
         </tr>
          <tr>  
          <td>Daily allowance :</td>  
          <td>${command.daily_allowance}</td>
         </tr>
          <tr>  
          <td>Total allowance :</td>  
          <td>${command.total_allowance}</td>
         </tr>
          <tr>  
          <td>Add transport cost :</td>  
          <td>${command.transport_cost}</td>
         </tr>
          <tr>  
          <td>Add accommodation costs :</td>  
          <td>${command.accommodation_costs}</td>
         </tr>
          <tr>  
          <td>Add other costs :</td>  
          <td>${command.other_costs}</td>
         </tr>
         <tr>  
          <td>Add comments :</td>  
          <td>${command.comments}</td>
         </tr>
         
          <tr>  
          <td></td><td><button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='/account/viewtrips'">Back</button></td>  
         </tr>  
                  
         </table>  
         </form:form> 
      
