package com.hellokoding.account.model;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Date;

public class Report {
	private int id; 
	private int employee_business_trip_id;
	private String number;
	private String description;
	private Blob image;
	private Date report_date;
	private boolean is_submitted;
	private boolean is_confirmed;
	private String comments_payout;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEmployee_business_trip_id() {
		return employee_business_trip_id;
	}
	public void setEmployee_business_trip_id(int employee_business_trip_id) {
		this.employee_business_trip_id = employee_business_trip_id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Blob getImage() {
		return image;
	}
	public void setImage(Blob image) {
		this.image = image;
	}
	public Date getReport_date() {
		return report_date;
	}
	public void setReport_date(Date report_date) {
		this.report_date = report_date;
	}
	public boolean isIs_submitted() {
		return is_submitted;
	}
	public void setIs_submitted(boolean is_submitted) {
		this.is_submitted = is_submitted;
	}
	public boolean isIs_confirmed() {
		return is_confirmed;
	}
	public void setIs_confirmed(boolean is_confirmed) {
		this.is_confirmed = is_confirmed;
	}
	public String getComments_payout() {
		return comments_payout;
	}
	public void setComments_payout(String comments_payout) {
		this.comments_payout = comments_payout;
	}

}
