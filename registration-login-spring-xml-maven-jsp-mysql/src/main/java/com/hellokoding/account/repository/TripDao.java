package com.hellokoding.account.repository;  
import java.io.ByteArrayInputStream;
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;

import com.hellokoding.account.model.Business_trip;
import com.hellokoding.account.model.Employee;  

  
public class TripDao {  
JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Business_trip p){  
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("employee_id", p.getEmployee_id());
		mapSqlParameterSource.addValue("description", p.getDescription());
		mapSqlParameterSource.addValue("start_date", p.getStart_date());
		mapSqlParameterSource.addValue("end_date", p.getEnd_date());
		mapSqlParameterSource.addValue("confirmed", p.isConfirmed());
		mapSqlParameterSource.addValue("number_of_days", p.getNumber_of_days());
		mapSqlParameterSource.addValue("number_of_night", p.getNumber_of_night());
		mapSqlParameterSource.addValue("daily_allowance", p.getDaily_allowance());
		mapSqlParameterSource.addValue("total_allowance", p.getTotal_allowance());
		mapSqlParameterSource.addValue("transport_cost", p.getTransport_cost());
		mapSqlParameterSource.addValue("accommodation_costs", p.getAccommodation_costs());
		mapSqlParameterSource.addValue("other_costs", p.getOther_costs());
		mapSqlParameterSource.addValue("comments", p.getComments());
		if(p.getPicture() != null) {
			mapSqlParameterSource.addValue("picture",  new SqlLobValue(new ByteArrayInputStream(p.getPicture()), 
		   p.getPicture().length, new DefaultLobHandler()), Types.BLOB);
		}
		else {
			mapSqlParameterSource.addValue("picture", null);
		}

	    NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
	    
		String sql = "insert into business_trip(employee_id, description, start_date, end_date, confirmed, "
				+ "number_of_days, number_of_night, daily_allowance,total_allowance, transport_cost, accommodation_costs, other_costs, comments, picture) "
				+ "VALUES (:employee_id, :description, :start_date, :end_date, :confirmed, :number_of_days, "
				+ ":number_of_night, :daily_allowance,:total_allowance, :transport_cost, :accommodation_costs, :other_costs, :comments, :picture)";
	  
	    return jdbcTemplateObject.update(sql, mapSqlParameterSource);  
 
	}  
	public int update(Business_trip p){  
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("id", p.getId());
		mapSqlParameterSource.addValue("employee_id", p.getEmployee_id());
		mapSqlParameterSource.addValue("description", p.getDescription());
		mapSqlParameterSource.addValue("start_date", p.getStart_date());
		mapSqlParameterSource.addValue("end_date", p.getEnd_date());
		mapSqlParameterSource.addValue("confirmed", p.isConfirmed());
		mapSqlParameterSource.addValue("number_of_days", p.getNumber_of_days());
		mapSqlParameterSource.addValue("number_of_night", p.getNumber_of_night());
		mapSqlParameterSource.addValue("daily_allowance", p.getDaily_allowance());
		mapSqlParameterSource.addValue("total_allowance", p.getTotal_allowance());
		mapSqlParameterSource.addValue("transport_cost", p.getTransport_cost());
		mapSqlParameterSource.addValue("accommodation_costs", p.getAccommodation_costs());
		mapSqlParameterSource.addValue("other_costs", p.getOther_costs());
		mapSqlParameterSource.addValue("comments", p.getComments());
			
		 NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
		 
		String sql = "UPDATE Business_trip SET employee_id =  :employee_id,description= :description,start_date= :start_date,"
				+ "end_date= :end_date,confirmed= :confirmed,number_of_days= :number_of_days,number_of_night= :number_of_night,"
				+ "daily_allowance= :daily_allowance,total_allowance= :total_allowance,transport_cost= :transport_cost,accommodation_costs = :accommodation_costs,"
				+ "other_costs= :other_costs,comments= :comments WHERE id = :id";
	 
		return jdbcTemplateObject.update(sql, mapSqlParameterSource);
	}  
	
	public int approve(int business_trip_id){  
		 String sql="update business_trip SET confirmed=1 where id=" +business_trip_id;
		 return template.update(sql);  
	}  
	
	
	public int delete(int id){  
	    String sql="delete from business_trip where id="+id+"";  
	    return template.update(sql);  
	}  
	public Business_trip getBusiness_tripById(int id){   // n�itab k�iki l�hetusi
	    String sql="select * from business_trip where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Business_trip>(Business_trip.class));  
	}  
	public List<Business_trip> getBusiness_trips(int employee_id){  
	    return template.query("select id, employee_id, description, start_date, end_date, confirmed, number_of_days,"
	    		+ " number_of_night, daily_allowance, total_allowance, transport_cost, accommodation_costs, other_costs, "
	    		+ "comments, picture from business_trip WHERE employee_id=" + employee_id + " AND start_date >= DATE_SUB(NOW(), INTERVAL 6 MONTH) ORDER BY start_date DESC LIMIT 20",new RowMapper<Business_trip>(){  
	    			
	    			
	        public Business_trip mapRow(ResultSet rs, int row) throws SQLException {  
	            Business_trip e=new Business_trip();  
	            e.setId(rs.getInt(1));  
	            e.setEmployee_id(rs.getInt(2));
	            e.setDescription(rs.getString(3));
	            e.setStart_date(rs.getDate(4));
	            e.setEnd_date(rs.getDate(5));
	            e.setConfirmed(rs.getBoolean(6)); 
	            e.setNumber_of_days(rs.getInt(7));
	            e.setNumber_of_night(rs.getInt(8));
	            e.setDaily_allowance(rs.getDouble(9));
	            e.setTotal_allowance(rs.getDouble(10));
	            e.setTransport_cost(rs.getDouble(11));
	            e.setAccommodation_costs(rs.getDouble(12));
	            e.setOther_costs(rs.getDouble(13));
	            e.setComments(rs.getString(14));
	            e.setPicture(rs.getBytes(15));
	            return e;  
	        }  
	    });  
	}  
	public List<Business_trip> getBusiness_trips(){  
	    return template.query("select id, employee_id, description, start_date, end_date, confirmed, number_of_days,"
	    		+ " number_of_night, daily_allowance, total_allowance, transport_cost, accommodation_costs, other_costs, "
	    		+ "comments, picture from business_trip ORDER BY start_date DESC",new RowMapper<Business_trip>(){  
	    			
	    			
	        public Business_trip mapRow(ResultSet rs, int row) throws SQLException {  
	            Business_trip e=new Business_trip();  
	            e.setId(rs.getInt(1));  
	            e.setEmployee_id(rs.getInt(2));
	            e.setDescription(rs.getString(3));
	            e.setStart_date(rs.getDate(4));
	            e.setEnd_date(rs.getDate(5));
	            e.setConfirmed(rs.getBoolean(6)); 
	            e.setNumber_of_days(rs.getInt(7));
	            e.setNumber_of_night(rs.getInt(8));
	            e.setDaily_allowance(rs.getDouble(9));
	            e.setTotal_allowance(rs.getDouble(10));
	            e.setTransport_cost(rs.getDouble(11));
	            e.setAccommodation_costs(rs.getDouble(12));
	            e.setOther_costs(rs.getDouble(13));
	            e.setComments(rs.getString(14));
	            e.setPicture(rs.getBytes(15));
	            return e;  
	        }  
	    });  
	}  
	
	public List<Business_trip> getAllBusiness_trips(int employee_id){  
		 return template.query("select id, employee_id, description, start_date, end_date, confirmed, number_of_days,"
		    		+ " number_of_night, daily_allowance, total_allowance, transport_cost, accommodation_costs, other_costs, "
		    		+ "comments, picture from business_trip WHERE employee_id=" + employee_id + " ORDER BY start_date DESC",new RowMapper<Business_trip>(){   
	    			
	    			
	        public Business_trip mapRow(ResultSet rs, int row) throws SQLException {  
	            Business_trip e=new Business_trip();  
	            e.setId(rs.getInt(1));  
	            e.setEmployee_id(rs.getInt(2));
	            e.setDescription(rs.getString(3));
	            e.setStart_date(rs.getDate(4));
	            e.setEnd_date(rs.getDate(5));
	            e.setConfirmed(rs.getBoolean(6)); 
	            e.setNumber_of_days(rs.getInt(7));
	            e.setNumber_of_night(rs.getInt(8));
	            e.setDaily_allowance(rs.getDouble(9));
	            e.setTotal_allowance(rs.getDouble(10));
	            e.setTransport_cost(rs.getDouble(11));
	            e.setAccommodation_costs(rs.getDouble(12));
	            e.setOther_costs(rs.getDouble(13));
	            e.setComments(rs.getString(14));
	            e.setPicture(rs.getBytes(15));
	            return e;  
	        }  
	    });  
	}  
	
}  