package com.hellokoding.account.web;   
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.User;
import com.hellokoding.account.repository.EmployeeDao;
import com.hellokoding.account.repository.UserRepository;
import com.hellokoding.account.service.UserService;   

@Controller  
public class EmployeeController {  
    @Autowired  
    EmployeeDao dao;//will inject dao from xml file  
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;

      
    /*It displays a form to input data, here "command" is a reserved request attribute 
     *which is used to display object data into form 
     */  
    @RequestMapping("/employeeform")  
    public String showform(Model m){ 
    	
    	Employee employee =  new Employee();
    	employee.setUser_id(userService.getLoggedInUser().getId());
    	m.addAttribute("command", employee);
    	m.addAttribute("users", usersToMap(userRepository.findAll()));
    	return "employeeform"; 
    }  
    /*It saves object into database. The @ModelAttribute puts request data 
     *  into model object. You need to mention RequestMethod.POST method  
     *  because default request is GET*/  
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("employee") Employee employee){  
        dao.save(employee);  
        return "redirect:/welcome";//will redirect to viewemployee request mapping  
    }  
    
    
    /* It provides list of employees in model object */  
    @RequestMapping("/viewemployee")  
    public String viewemployee(Model m){  
        List<Employee> list=dao.getEmployees();  
        m.addAttribute("list",list);
        boolean isAdmin =  userService.hasRole("ROLE_ADMIN");
        m.addAttribute("isAdmin", userService.hasRole("ROLE_ADMIN"));

        return "viewemployee";  
    }  
    /* It displays object data into form for the given id.  
     * The @PathVariable puts URL data into variable.*/  
    @RequestMapping(value="/editemployee/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Employee employee=dao.getEmployeeById(id);  
        m.addAttribute("command",employee);
        m.addAttribute("users", usersToMap(userRepository.findAll()));
        return "employeeeditform";  
    }  
    /* It updates model object. */  
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("employee") Employee employee){  
        dao.update(employee);  
        return "redirect:/viewemployee";  
    }  
    /* It deletes record for the given id in URL and redirects to /viewemployee */  
    @RequestMapping(value="/deleteemployee/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/viewemployee";  
    }   
    private Map<Long, String> usersToMap(List<User> users) {
    	Map<Long, String> usersMap = new HashMap<>();
    	
    	for(User user : users) {
    		usersMap.put(user.getId(), user.getUsername());
    	}
    	
    	return usersMap;
    }

}  