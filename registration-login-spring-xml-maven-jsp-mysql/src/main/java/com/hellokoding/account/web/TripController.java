package com.hellokoding.account.web;   
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hellokoding.account.model.Business_trip;
import com.hellokoding.account.model.Employee;
import com.hellokoding.account.repository.EmployeeDao;
import com.hellokoding.account.repository.TripDao;
import com.hellokoding.account.service.UserService;   

@Controller  
public class TripController {  
    @Autowired  
    TripDao dao;//will inject dao from xml file  
    @Autowired  
    EmployeeDao employeeDao;//will inject dao from xml file  
    
    @Autowired  
    UserService userService;
      
    /*It displays a form to input data, here "command" is a reserved request attribute 
     *which is used to display object data into form 
     */  
    @RequestMapping("/travel_info_form")  
    public String showform(Model m){  
    	Business_trip businessTrip = new Business_trip();
    	businessTrip.setEmployee_id(employeeDao.getEmployeeByUserId(userService.getLoggedInUser().getId()).getId());
    	m.addAttribute("command", businessTrip);
    	return "travel_info_form"; 
    }  
    /*It saves object into database. The @ModelAttribute puts request data 
     *  into model object. You need to mention RequestMethod.POST method  
     *  because default request is GET*/  
    @RequestMapping(value="/saveTrip",method = RequestMethod.POST)  
    public String save_trip(@ModelAttribute("business_trip") Business_trip business_trip,  
    	@RequestParam("file") MultipartFile file){
        	try {
    			if(file.getBytes().length > 0) {
    				business_trip.setPicture(file.getBytes());
    			}
    			
    			
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        business_trip.setDaily_allowance(50);
        business_trip.setNumber_of_night(business_trip.getNumber_of_days() - 1);
        business_trip.setTotal_allowance(business_trip.getDaily_allowance() * business_trip.getNumber_of_days());
        	
    	dao.save(business_trip);  
        return "redirect:/viewtrips";//will redirect to viewtrips request mapping  
    }  
    @RequestMapping(value="/saveTripApproved/{id}",method = RequestMethod.GET)  
    public String save_trip_approved(@PathVariable int id){
                	
    	dao.approve(id);
        return "redirect:/manager";//will redirect to viewtrips request mapping  
    }  
    
    
    
//  /* It deletes record for the given id in URL and redirects to /viewtrips */  
  @RequestMapping(value="/deletebusiness_trip/{id}",method = RequestMethod.GET)  
  public String deletebusiness_trip(@PathVariable int id){  
      dao.delete(id);  
      return "redirect:/viewtrips";  
  }  
    
    
    
    
    
    @RequestMapping(value = "/getPhoto/{id}")
   	public void getPhoto(HttpServletResponse response,
   			@PathVariable("id") int id) throws Exception {
   		response.setContentType("image/jpg");
   		 
   		Business_trip business_trip = dao.getBusiness_tripById(id);
   		byte[] bytes = business_trip.getPicture();
   		InputStream inputStream = new ByteArrayInputStream(bytes);
   		IOUtils.copy(inputStream, response.getOutputStream());
   	}

//    /* It provides list of trips in model object */  
    @RequestMapping("/viewtrips")  
    public String viewtrips(Model m){  
        List<Business_trip> list=dao.getAllBusiness_trips(employeeDao.getEmployeeByUserId(userService.getLoggedInUser().getId()).getId());  
        m.addAttribute("list",list);
        return "viewtrips";  
    } 
    
//  /* It provides list of employees in model object */  
  @RequestMapping("/viewemployeestrips")  
  public String viewemployeestrips(Model m){  
      List<Business_trip> list=dao.getBusiness_trips();  
      m.addAttribute("list",list);
      return "viewemployeestrips";  
  } 
    
//  /* It shows record for the given id in URL and redirects to /viewsingletrip */  
  @RequestMapping(value="/viewsingletrip/{id}",method = RequestMethod.GET)  
  public String viewsingletrip(@PathVariable int id, Model m){  
	  Business_trip business_trip=dao.getBusiness_tripById(id);  
	  
      m.addAttribute("command",business_trip);
      return "viewsingletrip"; // kui panna redirect, siis tekib tahtmatu ts�kkel ja hakkab iseendasse (v�i kuhu igane smuude meetodisse) suunama  
  } 
    
//    /* It displays object data into form for the given id.  
//     * The @PathVariable puts URL data into variable.*/  
    @RequestMapping(value="/tripeditform/{id}")  
    public String trip_edit(@PathVariable int id, Model m){  
        Business_trip business_trip=dao.getBusiness_tripById(id);  
        m.addAttribute("command",business_trip);
    //kui tahame m��rata check rolli m.addAttribute(""isAdmin", userService.hasRole("ROLE_ADMIN")); tagastab true v�i false
        return "tripeditform";  
    }  
    
//    /* It updates model object. */  
    @RequestMapping(value="/tripeditsave",method = RequestMethod.POST)  
    public String tripeditsave(@ModelAttribute("business_trip") Business_trip business_trip, @RequestParam("file") MultipartFile file){
    	try {
			if(file.getBytes().length > 0) {
				business_trip.setPicture(file.getBytes());
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        	
    	business_trip.setDaily_allowance(50);
    	business_trip.setNumber_of_night(business_trip.getNumber_of_days() - 1);
    	business_trip.setTotal_allowance(business_trip.getDaily_allowance() * business_trip.getNumber_of_days());
    	
        dao.update(business_trip);  
        return "redirect:/viewtrips";  
    }  
    
    
 
}  