-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: accounts
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `business_trip`
--

DROP TABLE IF EXISTS `business_trip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `business_trip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_confirmed` tinyint(1) NOT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `number_of_night` int(11) DEFAULT NULL,
  `daily_allowance` decimal(10,2) DEFAULT NULL,
  `transport_cost` decimal(10,2) DEFAULT NULL,
  `accommodation_costs` decimal(10,2) DEFAULT NULL,
  `other_costs` decimal(10,2) DEFAULT NULL,
  `comments` text,
  PRIMARY KEY (`id`),
  KEY `employee_business_trip_employee` (`employee_id`),
  CONSTRAINT `employee_business_trip_employee` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_trip`
--

LOCK TABLES `business_trip` WRITE;
/*!40000 ALTER TABLE `business_trip` DISABLE KEYS */;
INSERT INTO `business_trip` VALUES (3,6,'Lähetus seoses tööülesannetega','2019-01-02','2019-01-05',1,4,3,50.00,300.00,300.00,150.00,NULL),(5,7,'Lähetus seoses tööülesannetega','2019-02-01','2019-02-10',1,10,9,25.00,1000.00,1000.00,500.00,NULL);
/*!40000 ALTER TABLE `business_trip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp99`
--

DROP TABLE IF EXISTS `emp99`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `emp99` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `salary` decimal(10,0) DEFAULT NULL,
  `designation` varchar(60) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `trial` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp99`
--

LOCK TABLES `emp99` WRITE;
/*!40000 ALTER TABLE `emp99` DISABLE KEYS */;
/*!40000 ALTER TABLE `emp99` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_code` varchar(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_user` (`user_id`),
  CONSTRAINT `employee_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (6,'49001150256',1,4),(7,'38010101234',1,4),(8,'39012312345',1,4),(9,'46004161234',1,4),(10,'48805311222',1,4),(11,'45007162233',1,4),(12,'37009210123',1,4),(13,'38506231222',1,4),(14,'49007152333',1,4),(15,'39309299876',1,4),(16,'47002140123',1,4),(17,'48808121234',1,4),(18,'39003010456',1,4),(19,'37506167890',1,4),(20,'37605235678',1,4),(21,'38004153456',1,4),(22,'49008220345',1,4),(23,'47504120345',1,4),(24,'36703147654',1,4),(25,'47806300765',1,4);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_table`
--

DROP TABLE IF EXISTS `log_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `log_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `time` timestamp NOT NULL,
  `description` varchar(250) NOT NULL,
  `comments` text,
  PRIMARY KEY (`id`),
  KEY `log_table_employee` (`employee_id`),
  CONSTRAINT `log_table_employee` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_table`
--

LOCK TABLES `log_table` WRITE;
/*!40000 ALTER TABLE `log_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_business_trip_id` int(11) NOT NULL,
  `number` varchar(15) NOT NULL,
  `description` varchar(250) NOT NULL,
  `image` blob,
  `report_date` date NOT NULL,
  `is_submitted` tinyint(1) NOT NULL,
  `is_confirmed` tinyint(1) NOT NULL,
  `comments_payout` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_employee_business_trip` (`employee_business_trip_id`),
  CONSTRAINT `report_employee_business_trip` FOREIGN KEY (`employee_business_trip_id`) REFERENCES `business_trip` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_log_table`
--

DROP TABLE IF EXISTS `report_log_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `report_log_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `time` timestamp NOT NULL,
  `description` varchar(250) NOT NULL,
  `comments` text,
  PRIMARY KEY (`id`),
  KEY `report_log_table_employee` (`employee_id`),
  KEY `report_log_table_report` (`report_id`),
  CONSTRAINT `report_log_table_employee` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`),
  CONSTRAINT `report_log_table_report` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_log_table`
--

LOCK TABLES `report_log_table` WRITE;
/*!40000 ALTER TABLE `report_log_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_log_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_USER'),(2,'ROLE_ADMIN'),(3,'ROLE_MANAGER'),(4,'ROLE_HEAD_MANAGER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (4,'Piret11','$2a$11$KisKUL/2Y.sEhuWem.2KnOesmJz9i8k90hcmZx1TaNr6sUF/pcU/y',NULL,NULL),(5,'MariMaasikas','$2a$11$2GgaWi8SMLnm7jjLIcygIeBqDzs2SnhvdztTq3siY.1ham242SVva',NULL,NULL),(6,'MadisMurakas','$2a$11$f69bGOtjVHVVtsumLvYByOxSbi7dDLTCDd1fX0dOa9GuWejNbUUI6',NULL,NULL),(7,'KatiKuusk','$2a$11$ycl2ZSdONf36gck.I4c/h.Amzrhnoz0aZb6cYVZNzOpr4eUW53Uj2','Kati','Kuusk'),(8,'LiisiKala','$2a$11$1q6ecCyKdI22fJ..ve.K/OJSVuHCfBC/SJmNovWBeg3HMC3y1kVrC','Liisi','Kala'),(9,'PeeterPirukas','$2a$11$2E/YdRzaOqjKXkw8n29o2utx9HblD2u1vPeqMBMlAGPXeEVfWHcMe',NULL,NULL),(10,'LeidaLabidas','$2a$11$7XMdD2N0Itg3SxVuH.VibOl8Hu6bQvIGjLqgHs3Eps.pZ.e/DaTZe',NULL,NULL),(11,'ViiviSõnajalg','$2a$11$fivMefN3yZUaXopQyYV99OaQ9gzTqbF6DCB8IAiGdRwwf8DIM0Fzm',NULL,NULL),(12,'KaarelKask','$2a$11$dM.u/Znl6OqzUEpbPjsOceg5ZZMRS2mTfNYoKJ.dyxgtpD73rQHyq',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_role_roleid_idx` (`role_id`),
  CONSTRAINT `fk_user_role_roleid` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_role_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(7,2),(8,2),(9,2),(12,2),(7,3),(8,3),(9,3),(12,3),(7,4),(8,4),(9,4),(12,4);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-24 15:17:03
